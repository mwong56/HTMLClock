$(document).ready(function() {
  setInterval("getTime()", 1000);
  getTemp();
  
  Parse.initialize("RdSJmFigEEziA0D3RwGBiRMJ6WaKQnEdB7b2AlLg", "AtmFHvTl6xA2MLPitIhwYLcipAT8wDb09cyZ25kV");
  window.fbAsyncInit = function() {
    Parse.FacebookUtils.init({
      appId      : '327198160814246',
      xfbml      : true,
      version    : 'v2.2'
    });
  };

  (function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
   if (checkCurrentUser()) {
     showAddAlarms();
     getAllAlarms();
   } else {
     hideAddAlarms();
   }
});

function hideAddAlarms() {
  $("addAlarms").addClass("hide");
}

function showAddAlarms() {
  $("#addAlarms").removeClass("hide");
}

function checkCurrentUser() {
  var currentUser = Parse.User.current();
  if (currentUser) {
    console.log(Parse.User.current().id);
    $("#fbLogin").remove();
    return true;
  }
  return false;
}

function login() {
  Parse.FacebookUtils.logIn(null, {
        success: function(user) {
            if (!user.existed()) {
                $("#fbLogin").remove();
                showAddAlarms();
                getAllAlarms()
            } else {
                $("#fbLogin").remove();
                showAddAlarms();
                getAllAlarms()
            }
        },
        error: function(user, error) {
            alert("User cancelled the Facebook login or did not fully authorize.");
        }
    });
}
function getTime() {
  var date = new Date();
  var hour = date.getHours();
  var min = date.getMinutes();
  var sec = date.getSeconds();
  var doc = document.getElementById('myClock');
  doc.innerHTML = hour + ":" + min + ":" + sec;
}

function getTemp(position) {
  var forecastAPI = "https://api.forecast.io/forecast/bd99ea4402a1a0f0c614ae0ce9addb98/35.300399,-120.662362?callback=?"
  $.getJSON(forecastAPI, function (json) {
    var temp = json.currently.temperature;
    var sum = json.daily.summary;
    var icon =  "img/" + json.currently.icon + ".png";
    $("#forecastLabel").html(sum);
    $("#forecastIcon").attr("src", icon);
    if (temp < 60) {
      $("body").addClass("cold");
    } else if (temp >= 60 && temp < 70) {
      $("body").addClass("chilly");
    } else if (temp >= 70 && temp < 80) {
      $("body").addClass("nice");
    } else if (temp >= 80 && temp < 90) {
      $("body").addClass("warm");
    } else {
      $("body").addClass("hot");
    }
  });
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getTemp);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude; 
}

function showAlarmPopup() {
  $("#mask").removeClass("hide");
  $("#popup").removeClass("hide");
}

function hideAlarmPopup() {
  $("#mask").addClass("hide");
  $("#popup").addClass("hide");
}

function insertAlarm(time, alarmName, alarm) {
  var div = $("<div>").addClass("flexable");
	div.append($("<div>").addClass("name").html(alarmName + " "));
  div.append($("<div>").addClass("time").html(" " + time));
  var remButton = $("<input>").addClass("button").attr("type", "button").val("Delete");
  var myAlarm = alarm;
  remButton.click(function () {
    myAlarm.destroy({
      success: function() {
        ga('send', 'event', 'Alarm', 'Delete');
        div.remove();
      }
    });
  });
  
  div.append(remButton);
  $("#alarms").append(div);
}

function addAlarm() {
  var hours = $("#hours option:selected").text();
  var mins = $("#mins option:selected").text();
  var ampm = $("#ampm option:selected").text();
  var alarmName = $("#alarmName").val();
  var time = hours + ":" + mins + " " + ampm;
  
  var AlarmObject = Parse.Object.extend("Alarm");
  var alarmObject = new AlarmObject();
  alarmObject.save({"time": time,"alarmName": alarmName, "userId":Parse.User.current().id}, {
    success: function(object) {
      ga('send', 'event', 'Alarm', 'Add');
      insertAlarm(time, alarmName);
      hideAlarmPopup();
    }
  });

}

function getAllAlarms() {  
  var AlarmObject = Parse.Object.extend("Alarm");
  var query = new Parse.Query(AlarmObject);
  query.find({
      success: function(results) {
        for (var i = 0; i < results.length; i++) { 
          if (results[i].get("userId") === Parse.User.current().id) {
            insertAlarm(results[i].get("time"), results[i].get("alarmName"), results[i]);
          }
        }
      }
  });

}
  