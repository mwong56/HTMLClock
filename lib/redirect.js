function redirect_init() {
  // First, parse the query string
  var params = {}, queryString = location.hash.substring(1),
      regex = /([^&=]+)=([^&]*)/g, m;
  while (m = regex.exec(queryString)) {
    params[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
  }

  console.log(params['access_token']);

  window.opener.callbackFn(params['access_token']);

  window.close();
}